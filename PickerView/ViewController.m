//
//  ViewController.m
//  PickerView
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/24.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //程式一開始時設定(載入)資料
    dataArray=[NSArray arrayWithObjects:@"雞腿",@"魚排",@"排骨", nil];
    
    dataArray2=[NSArray arrayWithObjects:@"紅茶",@"綠茶",@"可樂", nil];
    
    dataArray3=[NSArray arrayWithObjects:@"沙拉",@"洋蔥",@"炒蛋", nil];
}

//實作來源:UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //回傳dataArray某一筆資料，由目前所走訪得列號決定
//    return [dataArray objectAtIndex:row];
    
    //每一個列上的標題文字，則由對應的資料陣列中的對應順序來決定
    if (component==0) {
        return [dataArray objectAtIndex:row];
    } else if(component==2){
        return [dataArray2 objectAtIndex:row];
    }else{
        return [dataArray3 objectAtIndex:row];
    }
    
}
//實作來源:UIPickerViewDataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3; //只有一個Component(一個子滾軸)就填入1
}
//實作來源:UIPickerViewDataSource
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    //請他去問資料陣列裡有幾筆資料
    //return [dataArray count];
    if (component==0) {
        return [dataArray count]; //第一個子滾軸對應dataArray
    } else if(component==1){
        return [dataArray2 count]; //第二個子滾軸對應dataArray2
    }else{
        return [dataArray3 count]; //第三個子滾軸對應dataArray3
    }
    //三個分別去問資料陣列裡有幾筆資料
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toPick:(id)sender {
    //設定label為dataArray某一筆資料，由pickerview哪一個列（row）只有一個Component所以指定0
    _labelShow.text = [dataArray objectAtIndex:[_pickerView selectedRowInComponent:0]];
    _labelShow2.text = [dataArray2 objectAtIndex:[_pickerView selectedRowInComponent:1]];
    _labelShow3.text = [dataArray3 objectAtIndex:[_pickerView selectedRowInComponent:2]];
    
    //由於我們實作UIPickerView兩個protocol所以要回到storyboard找到 Picker View設定Outlets底下的dataSource以及
    //delegate與View Controller發生關聯
}
@end
