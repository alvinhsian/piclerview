//
//  AppDelegate.h
//  PickerView
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/24.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

