//
//  ViewController.h
//  PickerView
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/24.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import <UIKit/UIKit.h>

//使用PickerView須遵循UIPickerViewDelegate, UIPickerViewDataSource這兩個protocol
@interface ViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>
{
    //這個iVar陣列用來儲存UIPickerView的資料
    NSArray *dataArray;
    //加入兩個陣列儲存配菜跟飲料
    NSArray *dataArray2;
    NSArray *dataArray3;
}

@property (weak, nonatomic) IBOutlet UILabel *labelShow;
@property (weak, nonatomic) IBOutlet UILabel *labelShow2;
@property (weak, nonatomic) IBOutlet UILabel *labelShow3;

- (IBAction)toPick:(id)sender;


@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@end

